    #pragma once
#include <iostream>
using namespace std;


template <class T> class myList {
	struct Node
	{
		T val;
		Node *next, *prev;
	};

	int size;
	Node *head, *tail;
public:
	myList();//Никита
	myList(const myList &);//Никита
	~myList();//Никита
	int size();//Руслан
	void clear();//Руслан
	bool is_empty();//Руслан
	bool value_is_find(T val);//Руслан
	T& at(pos);//Руслан
	bool change_value(int pos);//Руслан
	int get_position(T val);//Руслан
	void push_back(T val);//Руслан
	bool push_at_position(int pos);//Руслан
	bool del_at_value(T val);//Руслан
	bool del_at_position(int pos);//Руслан

	class Iterator {//Никита
	public:
		Iterator();
		T &operator *();
		Iterator &operator ++();
		Iterator &operator --();
		bool operator ==(const Iterator&);
		bool operator !=(const Iterator&);
	};
	Iterator begin();
	Iterator rbegin();
	Iterator end();
};
